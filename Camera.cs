﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDown
{
    public class Camera
    {
        public Matrix transform;
        Viewport view;
        Vector2 centre;

        public float zoom;

        public Camera(Viewport newView)
        {
            this.view = newView;
            this.zoom = 1f;
        }

        private void Input()
        {
            // Camera zoom
            if (Core.currentKeyboardState.IsKeyDown(Keys.OemPlus))
                this.zoom += 0.01f;
            if (Core.currentKeyboardState.IsKeyDown(Keys.OemMinus))
                this.zoom -= 0.01f;
        }

        public void Update(GameTime gameTime, Player player)
        {
            Input();
            if (this.zoom < 0) { this.zoom = 0; }

            centre = new Vector2(player.position.X + (player.texture.Width/2) - Core.bufferWidth/2 , player.position.Y + (player.texture.Height/2) - Core.bufferHeight/2);
            transform = Matrix.CreateScale(new Vector3(this.zoom, this.zoom, 0)) * 
                Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDown
{
    public class TileMap
    {
        public string[] readMap;
        public List<string> rawMap = new List<string>();
        public List<MapLayer> layerMap = new List<MapLayer>();

        public List<MapRow> Rows = new List<MapRow>();
        public int mapWidth = LoadMap.mapWidth;
        public int mapHeight = LoadMap.mapHeight / LoadMap.layerCount;

        public TileMap()
        {

        }

        public void WorldLoad()
        {
            for (int l = 0; l < LoadMap.layerCount; l++)
            {
                MapLayer layer = new MapLayer();
                for (int r = 0; r < 1; r++)
                {
                    MapRow row = new MapRow();
                    int x = 0;
                    int y = 0;
                    for (int c = 0; c < this.mapHeight * this.mapWidth; c++)
                    {                        
                        if (c != 0 && c % this.mapWidth == 0) {
                            layer.Rows.Add(row);
                            x = 0;
                            y++;
                            row = new MapRow();
                        } else if (c == this.mapHeight * this.mapWidth - 1) {
                            layer.Rows.Add(row);
                        }
                        MapCell cell = new MapCell(LoadMap.intMap[l][c]);
                        cell.layer = l;
                        cell.x = x * Core.currentWorld.gridSize;
                        cell.y = y * Core.currentWorld.gridSize;
                        row.Columns.Add(cell);
                        x++;
                    }
                }
                this.layerMap.Add(layer);
            }
        }

        public Rectangle GetSourceRectangle(int tileIndex)
        {
            int tileX = tileIndex % (Core.currentWorld.TileSetTexture.Width / Core.currentWorld.gridSize);
            int tileY = tileIndex / (Core.currentWorld.TileSetTexture.Width / Core.currentWorld.gridSize);

            return new Rectangle(tileX * Core.currentWorld.gridSize, tileY  * Core.currentWorld.gridSize, Core.currentWorld.gridSize, Core.currentWorld.gridSize);
        }
    }

    public class MapCell
    {
        public int TileID;

        public int drawLayer;
        public int layer;
        public int x;
        public int y;

        public int height;
        public int width;

        public bool iscollidable;
        public bool isLayerDynamic;
        public bool isFluid;

        public MapCell(int tileID)
        {
            this.TileID = tileID;
            this.height = Core.currentWorld.gridSize;
            this.width = Core.currentWorld.gridSize;
        }

    }

    public class MapRow
    {
        public List<MapCell> Columns = new List<MapCell>();
    }

    public class MapLayer
    {
        public List<MapRow> Rows = new List<MapRow>();
    }
        
}
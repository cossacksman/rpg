using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDown
{
    public class Core : Microsoft.Xna.Framework.Game
    {
        public static GraphicsDeviceManager graphics;

        public static int millisecondsPerFrame = 1;
        public static int timeSinceLastUpdate = 0;

        public static int bufferHeight;
        public static int bufferWidth;

        public static int windowHeight;
        public static int windowWidth;

        public static KeyboardState previousKeyboardState;
        public static KeyboardState currentKeyboardState;

        public static SpriteBatch spriteBatch;
        public static Texture2D playerSprites;
        public static SpriteFont debugFont;
        public static string debugString = "";

        public static Player player;

        public static World world1;
        public static World currentWorld;

        public static Camera camera;

        public Core()
        {
            // Create a 'Content' directory for files and a 
            // graphics devices manager
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
        }

        protected override void Initialize()
        {
            base.Initialize();

            windowHeight = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            windowWidth = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            bufferHeight = graphics.PreferredBackBufferHeight = windowHeight;
            bufferWidth = graphics.PreferredBackBufferWidth = windowWidth;

            base.IsFixedTimeStep = true;
            base.IsMouseVisible = false;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            // Debug init things...
            camera = new Camera(GraphicsDevice.Viewport);
            world1 = new World();
            world1.TileSetTexture = Content.Load<Texture2D>("Materials/Tilesets/tileset");
            currentWorld = world1;
            currentWorld.tileMap.WorldLoad();

            player = new Player(80f, 80f, 2, 1, Color.White);
            player.texture = playerSprites;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            debugFont = Content.Load<SpriteFont>("Fonts/debugFont");
            playerSprites = Content.Load<Texture2D>("Materials/Characters/player");
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            timeSinceLastUpdate += gameTime.ElapsedGameTime.Milliseconds;
            if (timeSinceLastUpdate >= millisecondsPerFrame)
            {
                timeSinceLastUpdate = 0;
                UpdateInput();           
            }

            world1.Update(gameTime);
            player.Update(gameTime);
            camera.Update(gameTime, player);
            
            base.Update(gameTime);
        }

        private void UpdateInput()
        {
            currentKeyboardState = Keyboard.GetState();
            // Close application upon 360 "Back" or Keyboard "Escape"
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || currentKeyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            player.Input();

            previousKeyboardState = currentKeyboardState;
        }

        protected override void Draw(GameTime gameTime)
        {
            // Clear the back buffer
            GraphicsDevice.Clear(Color.Transparent);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);
                world1.Draw();
                spriteBatch.DrawString(debugFont, debugString, new Vector2(player.position.X+10, player.position.Y-50), Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
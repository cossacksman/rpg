﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDown
{
    public class Player
    {
        public Rectangle boundingBox;
        public Texture2D texture;
        public Color color;

        public int height;
        public int width;

        public int currentLayer;
        public int currentRow;
        public int currentTile;                                                                                                                                                                         

        public int sprite;
        
        public Vector2 previousPosition;
        public Vector2 position;
        public Vector2 footPos;
        public Vector2 newPosition;
        public Vector2 velocity;
                
        public int stepSize = 4;

        public float distance;
        public Vector2 gridPos;

        public string facing;

        public string debugString = "";

        public Player(float x, float y, int height, int width, Color color)
        {
            this.height = Core.currentWorld.gridSize * height;
            this.width = Core.currentWorld.gridSize * width;
            this.sprite = 0;
            this.currentLayer = 2;
            this.velocity.X = 0;
            this.velocity.Y = 0;
            this.position.X = x;
            this.position.Y = y;
            this.color = color;
            this.facing = "down";
            //this.texture = new Texture2D(Core.graphics.GraphicsDevice, this.height, this.width, false, SurfaceFormat.Color);
            //Color[] colors = Enumerable.Range(0, this.height*this.width).Select(i => this.color).ToArray();
            //texture.SetData(colors);
        }

        public void Update(GameTime gameTime)
        {
            this.footPos.X = this.position.X + (this.width / 2);
            this.footPos.Y = this.position.Y + ((this.height / 2)+(this.height / 4));
            

            Collision();
            Movement(gameTime);
            getPlayerTile();
            SpriteControl();
            //this.debugString = footPos.X.ToString() + "\n" + footPos.Y.ToString();
        }

        private void SpriteControl()
        {
            switch (facing)
            {
                case "up":
                    this.sprite = 1;
                    break;

                case "down":
                    this.sprite = 0;
                    break;

                case "left":
                    this.sprite = 2;
                    break;

                case "right":
                    this.sprite = 3;
                    break;
            }
        }

        private void Movement(GameTime gameTime)
        {
            this.previousPosition.X = this.position.X;
            this.previousPosition.Y = this.position.Y;

            // Add velocity to position
            this.position.X += this.velocity.X;
            this.position.Y += this.velocity.Y;
            
            // Calculate distance walked given veolocity
            this.distance += this.velocity.X + this.velocity.Y;
                        
            // If distance travelled is stepsize
            if (Math.Abs(this.distance - this.stepSize) >= this.stepSize || Math.Abs(this.distance - this.stepSize) <= this.stepSize)
            {
                this.distance = 0;
                this.velocity.X = 0;
                this.velocity.Y = 0;                
            }

        }

        public void Collision()
        {
            // If about to move
            if (this.velocity.X > 0 || this.velocity.X < 0 || this.velocity.Y > 0 || this.velocity.Y < 0)
            {
                // Detect which direction velocity is being applied to
                if (this.velocity.X > 0)
                    newPosition.X = this.position.X + this.stepSize;
                if (this.velocity.X < 0)
                    newPosition.X = this.position.X - this.stepSize;
                if (this.velocity.Y > 0)
                    newPosition.Y = this.position.Y + this.stepSize;
                if (this.velocity.Y < 0)
                    newPosition.Y = this.position.Y - this.stepSize;

                // If new position will collide with an object
                /*foreach(WorldObject obj in Core.world1.worldObjects)
                {
                    if (obj.collidesAll)
                    {
                        if (Collisions.CollidesAll(this.texture, this.newPosition, obj) == true)
                        {
                            this.velocity.X = 0;
                            this.velocity.Y = 0;
                            this.newPosition = this.position;
                        }
                        else
                        {
                            // if not colliding
                        }
                    }
                }*/
            }
        }

        public void Input()
        {
            if (Core.currentKeyboardState.IsKeyDown(Keys.W))
            {
                this.previousPosition.Y = this.position.Y;
                this.facing = "up";
                this.velocity.Y = -this.stepSize;
            }
            if (Core.currentKeyboardState.IsKeyDown(Keys.A))
            {
                this.previousPosition.X = this.position.X;
                this.facing = "left";
                this.velocity.X = -this.stepSize;
            }
            if (Core.currentKeyboardState.IsKeyDown(Keys.S))
            {
                this.previousPosition.Y = this.position.Y;
                this.facing = "down";
                this.velocity.Y = this.stepSize;
            }
            if (Core.currentKeyboardState.IsKeyDown(Keys.D))
            {
                this.previousPosition.X = this.position.X;
                this.facing = "right";
                this.velocity.X = this.stepSize;
            }
        }

        public Rectangle getSourceRectangle(int tileIndex)
        {
            int tileX = tileIndex % (this.texture.Width / this.width);
            int tileY = tileIndex / (this.texture.Height / this.height);

            return new Rectangle(tileX * this.width, 0, this.width, this.height);
        }

        public void getPlayerTile()
        {
            // Update tile under player
            int ply = int.Parse(Core.player.position.Y.ToString());
            int plx = int.Parse(Core.player.position.X.ToString());
            for (int r = 0; r < Core.currentWorld.tileMap.layerMap[this.currentLayer].Rows.Count(); r++)
            {
                for (int c = 0; c < Core.currentWorld.tileMap.layerMap[this.currentLayer].Rows[r].Columns.Count(); c++)
                {
                    MapCell cell = Core.currentWorld.getCell(this.currentLayer, r, c);
                    plx = (int)Math.Ceiling(this.footPos.X);
                    ply = (int)Math.Ceiling(this.footPos.Y);
                    // Detect whether player is inside a tile
                    if (plx >= cell.x &&
                        plx <= cell.x + cell.width &&
                        ply >= cell.y &&
                        ply <= cell.y + cell.height)
                    {
                        this.currentTile = cell.TileID;
                    }
                }
            }
        }

        public void Draw()
        {
            Core.spriteBatch.Draw(this.texture, this.position, this.getSourceRectangle(this.sprite), this.color);
            Core.spriteBatch.DrawString(Core.debugFont, debugString, new Vector2(this.position.X + 10, this.position.Y - 50), Color.White);
        }
    }
}

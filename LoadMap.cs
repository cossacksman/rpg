﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace topDown
{
    public class LoadMap
    {
        public static List<string> rawMap = new List<string>();
        public static List<List<int>> intMap = new List<List<int>>();

        public static int mapHeight;
        public static int mapWidth;

        public static int layerCount;

        public static void ReadFile(string mapFile)
        {
            // Read initial map as strings from file into a string array
            string[] lines = System.IO.File.ReadAllLines(mapFile);
            foreach (string line in lines)
            {
                rawMap.Add(line);
                if (line == "#break" || line == "#start")
                {
                    // Replace the static values here (0) with total tileCount
                    intMap.Add(new List<int>());
                    layerCount++;
                }
            }

            // Layer iterator
            int l = 0;
            // Loop raw map and generate intager map array
            foreach (string line in rawMap)
            {
                // If line is not a layer divider, start or end of file
                if (line != "#break" && line != "#end" && line != "#start")
                {
                    string[] row = line.Split(',');
                    for (int i = 0; i < row.Length; i++)
                    {
                        int x = int.Parse(row[i]);
                        intMap[l].Add(x);
                    }
                    mapWidth = row.Length;
                    mapHeight++;
                }
                else if (line == "#break")
                {
                    l++;
                }
                else if (line == "#end")
                {
                    // Do something
                }
            }
        }
    }

    

}  
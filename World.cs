﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDown
{
    public class World
    {
        public Texture2D backgroundTexture;
        public Rectangle backgroundHolder;

        public Texture2D TileSetTexture;

        public int gridSize = 32;

        public TileMap tileMap;
        public int squaresDown;
        public int squaresAcross;

        public int layerCount;

        public World()
        {
            LoadMap.ReadFile("Content/Maps/map.txt");
            this.tileMap = new TileMap();
            this.squaresDown = this.tileMap.mapHeight;
            this.squaresAcross = this.tileMap.mapWidth;
            this.layerCount = this.tileMap.layerMap.Count();
        }

        public MapCell getCell(int l, int r, int c)
        {
            MapCell mapCell = Core.currentWorld.tileMap.layerMap[l].Rows[r].Columns[c];
            return mapCell;
        }

        public void Update(GameTime gameTime)
        {
            // Do nothing yet
        }

        public void Draw()
        {
            for (int l = 0; l < this.tileMap.layerMap.Count(); l++)
            {                
                for (int r = 0; r < this.squaresDown; r++)
                {
                    for (int c = 0; c < this.squaresAcross; c++)
                    {
                        Core.spriteBatch.Draw(
                            this.TileSetTexture,
                            new Rectangle((c * this.gridSize), (r * this.gridSize), this.gridSize, this.gridSize),
                            this.tileMap.GetSourceRectangle(this.tileMap.layerMap[l].Rows[r].Columns[c].TileID),
                            Color.White
                        );
                        this.tileMap.layerMap[l].Rows[r].Columns[c].x = c * this.gridSize;
                        this.tileMap.layerMap[l].Rows[r].Columns[c].y = r * this.gridSize;
                        if (this.tileMap.layerMap[l].Rows[r].Columns[c].y < (int)(Math.Ceiling(Core.player.footPos.Y))) { this.tileMap.layerMap[l].Rows[r].Columns[c].drawLayer = 0; }
                        else { this.tileMap.layerMap[l].Rows[r].Columns[c].drawLayer = 1; }
                    }
                }
                if (l == Core.player.currentLayer) { Core.player.Draw(); }
            }
        }
    }
}
